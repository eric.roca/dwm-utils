#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define BUFFER_SIZE 256
#define BATTERY_STATUS_PATH     "/sys/class/power_supply/BAT%d/status"
#define BATTERY_CAPACITY_PATH   "/sys/class/power_supply/BAT%d/capacity"


int getBatteryStatus(const int);
int getBatteryCharge(const int);
int processBatteryCharge(const int);

/**
 * Retrieves the battery status for the given battery number.
 *
 * @param battery The battery number.
 * @return The battery status:
 *         - 0: Full
 *         - 1: Charging
 *         - 2: Discharging or Not charging
 */
int getBatteryStatus(const int battery)
{
    const int maxRetries = 5;
    char battery_path[BUFFER_SIZE];
    snprintf(battery_path, BUFFER_SIZE, BATTERY_STATUS_PATH, battery);
    FILE *fp = fopen(battery_path, "r");
    if (fp == NULL) {
        fprintf(stderr, "Error opening %s: n", battery_path);
        exit(1);
    }

    // Read the file
    char status[BUFFER_SIZE];
    fgets(status, BUFFER_SIZE, fp);
    fclose(fp);

    // Returns
    switch (status[0]) {
        case 'F': // Full
            return 0;
            break;
        case 'C': // Charging
            return 1;
            break;
        case 'D': // Discharging
            return 2;
            break;
        case 'N': // Not charging
            return 2;
            break;
        default: // Other
            // Retry the specified number of times
            static int retries = 0;
            if (retries < maxRetries) {
                sleep(1);
                retries++;
                return getBatteryStatus(battery);
            }
            else {
                printf("Unable to retrieve battery status.\n");
                exit(1);
            }
            break;
    }
}

/**
 * Processes the battery charge level and returns the appropriate value.
 *
 * @param charge The battery charge level.
 * @return The processed battery charge:
 */
int getBatteryCharge(const int battery)
{
    char battery_path[BUFFER_SIZE];
    snprintf(battery_path, BUFFER_SIZE, BATTERY_CAPACITY_PATH, battery);
    FILE *fp = fopen(battery_path, "r");
    if (fp == NULL) {
        perror("Error opening battery capacity file");
        exit(1);
    }

    // Read the file
    char capacity[BUFFER_SIZE];
    fgets(capacity, BUFFER_SIZE, fp);
    fclose(fp);

    // Remove the trailing "\n" if present
    const size_t len = strlen(capacity);
    if (len > 0 && capacity[len - 1] == '\n') {
        capacity[len - 1] = '\0';
    }

    int charge = 0;
    sscanf(capacity, "%d", &charge);
    return charge;
}

/**
 * Process the battery charge level and return the appropriate value.
 *
 * @param charge The battery charge level.
 * @return The processed battery charge:
 *         - 0: Charge between 0 and 10%
 *         - 1: Charge between 10 and 25%
 *         - 2: Charge between 25 and 50%
 *         - 3: Charge between 50 and 75%
 *         - 4: Charge between 75 and 100%
 */
int processBatteryCharge(const int charge)
{
    if (charge >= 0 && charge <= 10) {
        return 0;
    } else if (charge > 10 && charge <= 25) {
        return 1;
    } else if (charge > 25 && charge <= 50) {
        return 2;
    } else if (charge > 50 && charge <= 75) {
        return 3;
    } else if (charge > 75 && charge <= 100) {
        return 4;
    } else {
        printf("Invalid battery charge: %d\n", charge);
        exit(1);
    }
}

int main()
{
    const int number_batteries = 1;
    const char *icons[3][5] = {
        {"󰁹", "󰁹", "󰁹", "󰁹", "󰁹"}, // Full
        {"󱃍", "󰢜", "󰂈", "󰂊", "󰂅"}, // Charging
        {"󱃍", "󰁻", "󰁽", "󰂁", "󰁹"}, // Discharging
    };

    int battery_charge = 0;
    for (int battery = 0; battery < number_batteries; battery++) {
        battery_charge = getBatteryCharge(battery);
        printf("%s %d%\n", icons[getBatteryStatus(battery)][processBatteryCharge(battery_charge)], battery_charge);
    }

    // Instead of calling system, we exec with a fork
    pid_t pid = fork();

    if (pid == 0) {
        execlp("pkill", "pkill", "-RTMIN+19", "dwmblocks", NULL);
        exit(0);
    }
    else if (pid == -1) {
        perror("Error forking.");
        exit(1);
    }
}
