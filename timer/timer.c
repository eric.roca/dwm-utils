#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

// Function to print the usage of the program and exit with an error code
void printUsage() {
    printf("Usage: mytimer -h hours -m minutes -s seconds\n");
    printf("where:\n");
    printf("    - hours indicates the number of hours\n");
    printf("    - minutes indicates the number of minutes\n");
    printf("    - seconds indicates the number of seconds\n");
    printf("After the specified time has elapsed, an alarm will be played\n");
    exit(1);
}

// Function to play the alarm sound
void playAlarm() {
    pid_t pid = fork();

    if (pid == 0) {
        execlp("paplay", "paplay", "/usr/share/sounds/freedesktop/stereo/complete.oga", NULL);
    } else if (pid == -1) {
        perror("Error forking.\n");
        exit(1);
    }
}

int main(int argc, char *argv[]) {
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    int hFlag = 0;
    int mFlag = 0;
    int sFlag = 0;
    int ret = 0;
    int opt;

    if (argc < 2) {
        printUsage();
    }

    // Parse command line options
    while ((opt = getopt(argc, argv, "h:m:s:")) != -1) {
        switch (opt) {
            case 'h':
                if (hFlag) {
                    printf("Error: duplicated -h option\n");
                    exit(1);
                }
                ret = sscanf(optarg, "%d", &hours);
                if (ret != 1 || hours < 0) {
                    printUsage();
                }
                hFlag = 1;
                break;
            case 'm':
                if (mFlag) {
                    printf("Error: duplicated -m option\n");
                    exit(1);
                }
                ret = sscanf(optarg, "%d", &minutes);
                if (ret != 1 || minutes < 0) {
                    printUsage();
                }
                mFlag = 1;
                break;
            case 's':
                if (sFlag) {
                    printf("Error: duplicated -s option\n");
                    exit(1);
                }
                ret = sscanf(optarg, "%d", &seconds);
                if (ret != 1 || seconds < 0) {
                    printUsage();
                }
                sFlag = 1;
                break;
            default:
                printUsage();
                break;
        }
    }

    // Calculate total seconds and sleep
    const int totalSeconds = 3600 * hours + 60 * minutes + seconds;
    sleep(totalSeconds);

    // Play alarm when the timer finishes
    playAlarm();

    return 0;
}
