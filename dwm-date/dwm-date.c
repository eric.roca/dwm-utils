#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

/**
 * @brief  Get the current date and time with an icon.
 *
 * This function gets the current date and time, formats it according to
 * the specified format, and prints it with an icon.
 *
 * @return void
 */
void get_date() {
    char date[50];
    const char* format = "%a, %d %b %H:%M";
    time_t t = time(NULL);
    strftime(date, sizeof(date), format, localtime(&t));
    printf("󰃰 %s\n", date);
}

int main(int argc, char *argv[]) {
    if (argc > 1 && strcmp(argv[1], "-n") == 0) {
      get_date();
    } else {
      // Calculate time until the next full minute
      time_t t_now = time(NULL);
      struct tm *tm_now = localtime(&t_now);
      int seconds_until_next_minute = 60 - tm_now->tm_sec;

      if (seconds_until_next_minute != 0) {
        // Sleep until the next full minute
        sleep(seconds_until_next_minute);

      }
      get_date();
    }
    return 0;
}
