#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INTERFACE_NAME_LENGTH 16
#define ROUTE_FILE "/proc/net/route"
#define NET_DEV_FILE "/proc/net/dev"
#define WIFI_FILE "/proc/net/wireless"

// Function to check if a string contains a substring
int containsSubstring(const char* str, const char* substr) {
    return strstr(str, substr) != NULL;
}

float getWirelessQuality(const char* interfaceName) {
    FILE* f = fopen(WIFI_FILE, "r");
    if (f == NULL) {
        perror("Failed to open WIFI_FILE");
        exit(1);
    }

    char line[256];
    while (fgets(line, sizeof(line), f) != NULL) {
        if (strstr(line, interfaceName) != NULL) {
            fgets(line, sizeof(line), f);
            float quality;
            sscanf(line + 17, "%f", &quality);
            fclose(f);
            return quality / 70.0;
        }
    }

    fclose(f);
    return -1.0; // Signal quality not found
}

// Function to check if a WireGuard interface is present
int isWireGuardInterface(const char* interfaceName) {
    FILE* f = fopen(NET_DEV_FILE, "r");
    if (f == NULL) {
        perror("Failed to open NET_DEV_FILE");
        exit(1);
    }

    char line[256];
    while (fgets(line, sizeof(line), f) != NULL) {
        if (containsSubstring(line, interfaceName)) {
            fclose(f);
            return 1; // WireGuard interface found
        }
    }

    fclose(f);
    return 0; // WireGuard interface not found
}

// Function to read the interface name from the routing table
void getInterfaceName(char* interfaceName) {
    FILE* f = fopen(ROUTE_FILE, "r");
    if (f == NULL) {
        perror("Failed to open ROUTE_FILE");
        exit(1);
    }

    char line[256];
    while (fgets(line, sizeof(line), f) != NULL) {
        if (containsSubstring(line, "\t00000000\t")) {
            sscanf(line, "%s", interfaceName);
            break;
        }
    }

    fclose(f);
}

int main() {
    char interfaceName[INTERFACE_NAME_LENGTH];
    getInterfaceName(interfaceName);

    const char *icons[][2] = {
      {"󰤨", "󰤪"},
      {"󰤢", "󰤤"},
      {"󱂇", "󰒄"},
      {"󰒎", "󰒎"},
    };

    int selected_icon_row = 0;
    int secure_connection = 0;
    switch(interfaceName[0]) {
        case 'w': // Wireless
            float quality = getWirelessQuality(interfaceName);
            if (quality >= 0.5) {
                selected_icon_row = 0;
            } else {
                selected_icon_row = 1;
            }
            break;
        case 'e': // Ethernet
            selected_icon_row = 2;
            break;
        case 't': // Tunnel
            secure_connection = 1;
        default: // Other
            selected_icon_row = 3;
            break;
    }

    secure_connection = secure_connection == 0 ? isWireGuardInterface("wg") : 1;
    printf(" %s", icons[selected_icon_row][secure_connection]);

    // Signal dwmblocks to update the statusbar
    system("pkill -RTMIN+17 dwmblocks");

    return 0;
}
